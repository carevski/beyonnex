package org.carevski.beyonnex;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class AnagramMachineImplTest {

    private final AnagramMachineImpl anagramMachine = new AnagramMachineImpl();

    @BeforeEach
    public void beforeEach() {
        anagramMachine.reset();
    }

    @Test
    void compareEdgeCases() {
        assertThrows(NullPointerException.class, () -> anagramMachine.compare(null, "asdf"));
        assertThrows(NullPointerException.class, () -> anagramMachine.compare("asdf", null));
        assertThrows(IllegalArgumentException.class, () -> anagramMachine.compare("", "asdf"));
        assertThrows(IllegalArgumentException.class, () -> anagramMachine.compare("asdf", ""));
    }

    @ParameterizedTest
    @CsvSource({
            "'AA ',' AA', true",    // should be
            "'AA  ',' AA', false",  // empty spaces make a difference
            "'aa','AA',false",       // casing makes a difference
            "'AABBCC','CCBBAA',true",
            "'resistance gainly thysanopteran','ancestries laying parasyntheton',true"
    })
    void compare(String one, String two, String result) {
        assertEquals(result, String.valueOf(anagramMachine.compare(one, two)));
    }

    @Test
    void findEdgeCase() {
        assertThrows(NullPointerException.class, () -> anagramMachine.find(null));
        assertThrows(IllegalArgumentException.class, () -> anagramMachine.find(""));
        assertTrue(anagramMachine.find("nothing to be found").isEmpty());
    }

    @Test
    void findTestOne() {
        //first set, find by spear
        assertTrue(anagramMachine.compare("pares", "parse"));
        assertTrue(anagramMachine.compare("pears", "reaps"));
        assertTrue(anagramMachine.compare("pears", "spare"));
        //second set, find by siren
        assertFalse(anagramMachine.compare("reins", "spare"));
        assertTrue(anagramMachine.compare("reins", "resin"));
        assertTrue(anagramMachine.compare("rinse", "risen"));
        //now let's find

        var setOne = anagramMachine.find("spear");
        assertTrue(setOne.containsAll(List.of("pares", "parse", "pears", "reaps", "spare")));
        var setTwo = anagramMachine.find("siren");
        assertTrue(setTwo.containsAll(List.of("reins", "resin", "rinse", "risen")));
        var setThree = anagramMachine.find("reins"); //this is already indexed so should be excluded from resutls
        assertTrue(setThree.containsAll(List.of("resin", "rinse", "risen")));

        assertTrue(anagramMachine.find("thisDoesNOtMatchEnyathing").isEmpty());
    }
}