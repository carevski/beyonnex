package org.carevski.beyonnex;

import java.util.*;

public interface AnagramMachine {

    /**
     * Compares two strings to check if they are anagrams.
     * </br>
     * Apart from just comparing the texts are also stored
     *
     * @param textOne - first text
     * @param textTwo - second text
     * @return true if they are anagrams, false otherwise
     */
    boolean compare(String textOne, String textTwo);

    /**
     * Looks up in the store to find anagrams that were cataloged from before
     *
     * @param text - text whose anagrams we need to find
     * @return set of anagrams that match given text
     */
    Set<String> find(String text);

    /**
     * Forger about any historical data.
     */
    void reset();
}
