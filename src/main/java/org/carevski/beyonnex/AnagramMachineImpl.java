package org.carevski.beyonnex;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * This particular implementation checks if two strings are anagram by counting&comparing character frequency.
 * <br>
 * Another option is to sort the strings and compare sorted version but would be less performant
 * <br>
 * Here we are implementing an interface, which means we can have other implementations too.
 */
public class AnagramMachineImpl implements AnagramMachine {

    private final Map<String, Set<String>> catalog = new ConcurrentHashMap<>();

    /**
     * Compares two strings to check if they are anagrams.
     * </br>
     * Apart from just comparing the texts are also stored
     *
     * @param textOne - first text
     * @param textTwo - second text
     * @return true if they are anagrams, false otherwise
     */
    public boolean compare(String textOne, String textTwo) {
        assertNotBlank(textOne);
        assertNotBlank(textTwo);
        return computeAndCatalog(textOne).equals(computeAndCatalog(textTwo));
    }

    public Set<String> find(String text) {
        assertNotBlank(text);
        var signature = signature(text);
        catalog.putIfAbsent(signature, new TreeSet<>());
        Set<String> candidates = catalog.get(signature);
        candidates.remove(text);
        return candidates;
    }

    @Override
    public void reset() {
        catalog.clear();
    }

    private void assertNotBlank(String textOne) {
        Objects.requireNonNull(textOne, "Text one can not be null");
        if (textOne.isBlank()) {
            throw new IllegalArgumentException("Text can not be blank string");
        }
    }

    private String computeAndCatalog(String text) {
        var signature = signature(text);
        catalog.putIfAbsent(signature, new TreeSet<>());
        catalog.get(signature).add(text);
        return signature;
    }

    /**
     * Blanks characters are treated just as any other character.
     * The algoritm counts character frequency and generates a signature of the text
     *
     * @param text - text where we should compute signature
     * @return signature for string
     */
    private String signature(String text) {
        Map<Character, Integer> freq = new TreeMap<>();
        text.chars().forEachOrdered(c -> {
            char character = (char) c;
            freq.putIfAbsent(character, 0);
            freq.put(character, freq.get(character) + 1);
        });
        return freq.entrySet()
                .stream()
                .map((e) -> "%s:%s".formatted(e.getKey(), e.getValue()))
                .collect(Collectors.joining(","));
    }

}
