import org.carevski.beyonnex.AnagramMachineImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) {
        var machine = new AnagramMachineImpl();
        while (true) {
            System.out.println("""
                    Choose one option:
                    1. Check if two text are anagrams;
                    2. Check all available anagrams for a text;
                    3. Exit;
                    Your Response:
                    """);

            String choice = readLine();
            switch (choice.trim()) {
                case "1" -> {
                    var lineOne = readLineFromConsole("1:");
                    var lineTwo = readLineFromConsole("2:");
                    var anagrams = machine.compare(lineOne, lineTwo);
                    System.out.println("The two text are anagrams: " + anagrams);
                }
                case "2" -> {
                    var text = readLineFromConsole("1:");
                    var anagrams = machine.find(text);
                    System.out.println("Available anagrams are: " + anagrams);
                }
                case "3" -> System.exit(1);
            }
        }
    }

    private static String readLineFromConsole(String line) {
        while (true) {
            System.out.println("Input your text %s".formatted(line));
            String text = readLine();
            if (null != text && !text.trim().isBlank()) {
                return text;
            }
        }
    }

    private static String readLine() {
        try {
            InputStreamReader streamReader = new InputStreamReader(System.in);
            BufferedReader bufferedReader = new BufferedReader(streamReader);
            return bufferedReader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}
