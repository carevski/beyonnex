# Notes
- The project users Java 21 and maven
- The app provides very simple CLI that you can run via Main class
- The anagram checker is considering space characters as standalone character
- The anagram checker is case-sensitive
- To run the test run

```shell
mvn clean test
```